#!/bin/sh

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

echo "Create template_chouette template"

# Create the 'template_chouette' template db
"${psql[@]}" <<- 'EOSQL'
  CREATE DATABASE template_chouette;
  UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template_chouette';
EOSQL

for DB in template_chouette "$POSTGRES_DB"; do
  echo "Loading Chouette extensions into $DB"

 	"${psql[@]}" --dbname="$DB" <<-'EOSQL'
  CREATE SCHEMA shared_extensions;
  GRANT ALL ON SCHEMA shared_extensions TO PUBLIC;
  CREATE EXTENSION postgis WITH SCHEMA shared_extensions;

  CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
  CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA shared_extensions;
  CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA shared_extensions;
EOSQL
done
